//1. insert one

db.users.insert({
	name:"single",
	accomodates: 2,
	price: 1000,
	description:"A simple room with all the basic necessities",
	rooms_available:10,
	isAvailable:false
	
})


//2.insert Many
db.users.insertMany([
		{
			name:"double",
			accomodates:3,
			price:2000,
			description:"A room fit for a small family going on a vacation",
			rooms_available:5,
			isAvailable:false
		},

		{
			name:"queen",
			accomodates:4,
			price:4000,
			description:"A room with a queen sized bed perfect for a simple getaway",
			rooms_available:15,
			isAvailable:false

		}

])

//3.find method

db.users.find({
	name:"double"
})

//4. updateOne method

db.users.updateOne(
		{
			name:"queen"
		},
		{
			$set:{
				rooms_available:0,
			}
		}

)


//5. deleteMany
db.users.deleteMany({
	rooms_available:0
})